<?php
    include_once('config.php');
	if (!array_key_exists('token', @$_SESSION))
	{
		header("Location: /"); 
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Tactics 1994</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/classic.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }

        .span4 .text {
            overflow: scroll;
        }

        .brand {
            padding: 0 10px !important;
        }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a id="headLink" class="label label-success" href="/user.php">User panel</a>
                <span class="divider"> / </span>
                <a class="brand" class="brand" href="/">Main page</a>
                <a id="logOut" style="float:right;" class="brand" href="/logout.php">Logout</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="hero-unit">
            <h1>Manage your profile</h1>
            <p>Update secret: <br />
                <input id="newSecret" name="newSecret" type="text" class="input-block-level" placeholder="new secret">
            </p>
            <p><a id="updateSecret" href="#" class="btn btn-primary btn-large">Update</a></p>
            <p><a id="recoverSecret" href="#" class="btn btn-primary btn-large">Recover</a></p>
        </div>
        <!-- Example row of columns -->
        <div class="row">
            <div class="span">
                <h2>Raw data</h2>
                <p id="liveSessions" class="text">Tell data from this form to your sysadmin. </p>
            </div>
        </div>
        <div class="row">
            <p class="text">
                <div id="msgsColumns"></div>
            </p>
            <p><a id="msgsUsers" class="btn" href="#">Update</a></p>
            <input id="textMessage" type="text" placeholder="Message" required>
            <input id="nicknameTo" type="text" placeholder="Nickname" required>
            <button id="sendMessage" type="submit" class="btn btn-primary">Send</button>
        </div>
        <hr>
        <footer>
            <p>&copy; Tactics 1994</p>
        </footer>
    </div> <!-- /container -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <script src="../assets/js/jquery.columns.min.js"></script>
    <script>
    $("#recoverSecret").click(function() {
        $.get("/actions.php?cmd=getPrintableSecret", function(data) {
            if (data.includes("<body>"))
                return alert('error');
            $("#liveSessions").html(data);
        });
    });

    $("#updateSecret").click(function() {
        if ($("#newSecret").val().length < 12) {
            $("#status").html("Secret's len > 11 only!");
            return;
        }

        $.post("/actions.php?cmd=setSharedSecret", { newSecret: $("#newSecret").val(), })
            .done(function(data) {});
    });

    function updateChat() {
        $.ajax({
            url: '/actions.php?cmd=getWallMessages',
            dataType: 'json',
            success: function(json) {
                cols = $('#msgsColumns').columns({
                    data: json["result"],
                });
                $('#msgsColumns').columns('resetData');
            }
        });
    }

    $("#msgsUsers").click(updateChat());

    $("#sendMessage").click(function() {
        $.post("/actions.php?cmd=addWallMessage", { to: $("#nicknameTo").val(), text: $("#textMessage").val(), })
            .done(function(data) {
                updateChat();
            });
    });
    </script>
</body>

</html>