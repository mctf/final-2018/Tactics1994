<?php
    include_once('config.php')
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Sign in &middot; Tactics 1994</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/classic.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            border: 1px solid #e5e5e5;
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

        .brand {
            padding: 0 10px !important;
        }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php if ( !isset($_SESSION["token"]) || empty($_SESSION["token"]) ) { ?>
                    <a id="headLink" class="label label-success" href="/">Sign in</a>
                    <span class="divider"> / </span>
                    <a id="signUp" class="brand" href="/signup.php">Sign Up</a>
                <?php } else { ?>
                    <a id="headLink" class="brand" href="/user.php">User panel</a>
                    <span class="divider"> / </span>
                    <a id="signUp" class="label label-success" href="/">Main page</a>
                    <a id="logOut" style="float:right;" class="brand" href="/logout.php">Logout</a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container">
    
    <?php if ( !isset($_SESSION["token"]) || empty($_SESSION["token"]) ) { ?>
        <form id="signinForm" class="form-signin" action="/actions.php?cmd=check2FAKey" method="POST">
            <h2 class="form-signin-heading">Please sign in</h2>
            <input name="name" type="text" class="input-block-level" placeholder="Name">
            <input name="key" type="text" class="input-block-level" placeholder="Code">
            <button class="btn btn-large btn-primary" type="submit">Sign in</button>
            <div id="lostAuth" class="btn btn-large btn-primary" type="submit">Get OTP</div>
        </form>
    <?php } else {?>
        <div id="usersForm" style="max-width: 75%;" class="form-signin">
            <h2>Users</h2>
            <p class="text">
                <div id="usersColumns"></div>
            </p>
            <p><a id="updateUsers" class="btn" href="#">Update</a></p>
        </div>
    <?php } ?>
    </div> <!-- /container -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <script src="../assets/js/jquery.columns.min.js"></script>
    <script>
    function getCookie(c_name) {
        var c_value = document.cookie,
            c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) c_start = c_value.indexOf(c_name + "=");
        if (c_start == -1) {
            c_value = null;
        } else {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    }

    <?php if ( !isset($_SESSION["token"]) || empty($_SESSION["token"]) ) { ?>
        $("#lostAuth").click(function() {
            //$("#dialog").dialog({modal: true, height: 590, width: 1005 });
            var w = window.open("/lostAuth.php", "popupWindow", "width=600, height=400, scrollbars=yes");
            var $w = $(w.document.body);
            $w.html("<textarea></textarea>");
        });
    <?php } else {?> 
        $("#updateUsers").click(function() {
            $.ajax({
                url: '/actions.php?cmd=getUsersList',
                dataType: 'json',
                success: function(json) {
                    cols = $('#usersColumns').columns({
                        data: json["result"],
                    });
                }
            });
        });
    <?php } ?>
    </script>
</body>

</html>