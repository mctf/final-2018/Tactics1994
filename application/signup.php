<?php
    include_once('config.php')
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Sign in &middot; Tactics 1994</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            border: 1px solid #e5e5e5;
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }
        
        .brand {
            padding: 0 10px !important;
        }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a id="headLink" class="brand" href="/">Sign in</a>
                <span class="divider"> / </span>
                <a id="headLink" class="label label-success" href="/signup.php">Sign up</a>
            </div>
        </div>
    </div>
    <div class="container">
        <form class="form-signin" action="/actions.php?cmd=signUp" method="POST">
            <h2 class="form-signin-heading">Please sign up</h2>
            <input id="name" name="name" type="text" class="input-block-level" placeholder="Name">
            <input id="secret" name="secret" type="text" class="input-block-level" placeholder="Secret">
            <div id="signUp" class="btn btn-large btn-primary" type="submit">Sign up</div>
            <br /><span id="status" style="padding: 0 0; margin: 10px 0 0 0; display: inline-block;" class="center label label-important"></span>
        </form>
    </div> <!-- /container -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <script>
    $("#signUp").click(function() {
        if ($("#secret").val().length < 12) {
            $("#status").html("Secret's len > 11 only!");
            return;
        }
        $.post("/actions.php?cmd=signUp", { name: $("#name").val(), secret: $("#secret").val(), })
            .done(function(data) {
                if (!JSON.parse(data)['result'])
                    $("#status").html("User exist!");
                else
                    $(location).attr("href", "/");
            });
    });
    </script>
</body>

</html>