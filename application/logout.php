<?php
	include_once('config.php');

	session_unset();
	session_destroy();
	$_SESSION = NULL;

	session_regenerate_id(true);
	
	header('Location: /');
?>