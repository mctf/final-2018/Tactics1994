<?php
	
	include_once('config.php');
	
	$pdo = NULL;
	
	function sendData($msg)
	{
		$sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
		$len = strlen($msg);
		
		socket_sendto($sock, $msg, $len, 0, 'Tactics1994-bin', 8843);
		sleep(1);
		socket_set_option($sock,SOL_SOCKET, SO_RCVTIMEO, array("sec"=>2, "usec"=>500));
		$result = socket_read($sock, 4096);
		
		socket_close($sock);
		return $result;
	}
	
	function initDBConnection()
	{
		global $pdo;
		$host = 'Tactics1994-db';
		$db   = 'tactics1994';
		$user = 'root';
		$pass = 'a4nIfNgCiR0z1VTjq8fM';
		$charset = 'utf8mb4';

		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$options = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$pdo = new PDO($dsn, $user, $pass, $options);
	}
	
	function getLastSecret()
	{
		global $pdo;

		$stm = $pdo->prepare('SELECT `result` FROM `sync_logs` ORDER BY `added` DESC LIMIT 1');
		$stm->execute();
		return $stm->fetch()['result'];
	} 
	
	function getSessions()
	{
		global $pdo;
		
		$stm = $pdo->prepare('SELECT * FROM `sessions`');
		$stm->execute();
		return $stm->fetchAll();
	}
	
	function getLoginAttemps()
	{
		global $pdo;
		
		$stm = $pdo->prepare('SELECT * FROM `req_logs`');
		$stm->execute();
		return $stm->fetchAll();
	}
	
	function getBackendResponses()
	{
		global $pdo;
		
		$stm = $pdo->prepare('SELECT * FROM `sync_logs`');
		$stm->execute();
		return $stm->fetchAll();
	}
	
	function makeToken($secret)
	{
		return md5($secret . time());
	}
	
	function saveToken($token, $userId)
	{
		global $pdo;
		$stm = $pdo->prepare("INSERT INTO `sessions` (`session`, `user_id`) VALUES (:session, :user_id)");
		$stm->bindParam(':session', $token);
		$stm->bindParam(':user_id', $userId);
		return $stm->execute();
	}
	
	function checkToken($token) 
	{
		global $pdo;
		$stm = $pdo->prepare('SELECT `user_id` FROM `sessions` WHERE `session` = :session ORDER BY `added` DESC LIMIT 1');
		$stm->bindParam(':session', $token);
		return ($stm->execute() !== false ? ($stm->fetch()['user_id']) : false);
	}
	
	function checkTokenAdmin($token)
	{
		global $pdo;
		$stm = $pdo->prepare('SELECT `user_id` FROM `sessions` WHERE `session` = :session ORDER BY `added` DESC LIMIT 1');
		$stm->bindParam(':session', $token);
		return ($stm->execute() !== false ? (getUserById($stm->fetch()['user_id'])['group'] == 2) : false);
	}
	
	function getUserByName($name)
	{
		global $pdo;
		$stm = $pdo->prepare("SELECT * FROM `users` WHERE `name` = :name ORDER BY `added` LIMIT 1");
		$stm->bindParam(':name', $name);
		$stm->execute();
		return $stm->fetch();
	}
	
	function loadWall($userid)
	{
		global $pdo;
		$stm = $pdo->prepare("SELECT * FROM `wall` WHERE (`to` LIKE ".$userid." OR `from`= :from) LIMIT 0, 1000");
		$stm->bindParam(':from', $userid);
		$stm->execute();
		$result = $stm->fetchAll();
		for ($i = 0; $i < count($result); ++$i)
		{
			$result[$i]['from'] = getUserById($result[$i]['from'])['name'];
			$result[$i]['to'] = getUserById($result[$i]['to'])['name'];
		}
		return $result;
	}
	
	function addWall($from, $to, $text)
	{
		global $pdo;
		$stm = $pdo->prepare("INSERT INTO `wall` (`from`, `to`, `text`) VALUES (:from, :to, :text)");
		$stm->bindParam(':from', 	$from);
		$stm->bindParam(':to', 		$to);
		$stm->bindParam(':text', 	$text);
		return $stm->execute();
	}
	
	function loadUsersList()
	{
		global $pdo;
		$stm = $pdo->prepare('SELECT `id`, `name` FROM `users` ORDER BY `added` DESC');
		$stm->execute();
		return $stm->fetchAll();
	}
	
	function getUserById($id)
	{
		global $pdo;
		$stm = $pdo->prepare("SELECT * FROM `users` WHERE `id` = :id ORDER BY `added` LIMIT 1");
		$stm->bindParam(':id', $id);
		$stm->execute();
		return $stm->fetch();
	}
	
	function getUserByToken($token)
	{
		$id = checkToken($token);
		if (!$id)
			return false;
		
		global $pdo;
		$stm = $pdo->prepare("SELECT * FROM `users` WHERE `id` = :id ORDER BY `added` LIMIT 1");
		$stm->bindParam(':id', $id);
		return $stm->execute() !== false ? $stm->fetch() : false;
	}
	
	function check2FAKey()
	{
		initDBConnection();
		$user = getUserByName($_POST['name']);
		$result = false;

		if($user)
		{
			$key = $_POST['key'];
			$secret = ((array_key_exists('secret', $_POST) && strlen(@$_POST['secret']) > 3) ? $_POST['secret'] : $user['secret']); 
			$data = "check2FAKey|{$key}|{$secret}|";
			$result = ($key == "05D0" || !(sendData($data) == 'bad'));
			
			if($result)
			{
				$_SESSION['token'] = makeToken($secret);
				saveToken($_SESSION['token'], $user['id']);
				if ($user['group'] == 2)
					header('Location: /admin.php');
				else
					header('Location: /user.php');
			}
		}
		header('Location: /');
		return json_encode(["result" => $result]);
	}
	
	function logSync($data)
	{
		global $pdo;
		$stm = $pdo->prepare("INSERT INTO `sync_logs` (`result`) VALUES (:data)");
		$stm->bindParam(':data', $data);
		return $stm->execute();
	}
	
	function addUser($name, $secret)
	{
		global $pdo;
		$stm = $pdo->prepare("INSERT INTO `users` (`name`, `secret`) VALUES (:name, :secret)");
		$stm->bindParam(':name', $name);
		$stm->bindParam(':secret', $secret);
		return $stm->execute();
	}
	
	function updateSharedSecret()
	{
		initDBConnection();
		$result = false;
		if(array_key_exists('token', $_SESSION) && checkTokenAdmin($_SESSION['token']))
		{
			$secret = sendData("getCurrentSecret|raw");
			if($secret != "bad")
				$result = logSync($secret);
		}
		return json_encode(["result" => $result]);
	}
	
	function setSharedSecret()
	{
		initDBConnection();
		$result = false;
		if(array_key_exists('token', $_SESSION) && checkTokenAdmin($_SESSION['token']))
		{
			$secret = sendData("setCurrentSecret|" . $_POST['newSecret']);
			if($secret != "bad")
				$result = logSync($_POST['newSecret']);
		}
		return json_encode(["result" => $result]);
	}
	
	function getListSessions()
	{
		initDBConnection();
		$result = false;
		if(array_key_exists('token', $_SESSION) && checkTokenAdmin($_SESSION['token']))
		{
			return json_encode(getSessions());
		}
		return json_encode(["result" => $result]);
	}
	
	function getListLoginAttemps()
	{
		initDBConnection();
		$result = false;
		if(array_key_exists('token', $_SESSION) && checkTokenAdmin($_SESSION['token']))
		{
			return json_encode(getLoginAttemps());
		}
		return json_encode(["result" => $result]);
	}
	
	function getListBackendResponses()
	{
		initDBConnection();
		$result = false;
		if(array_key_exists('token', $_SESSION) && checkTokenAdmin($_SESSION['token']))
		{
			return json_encode(getBackendResponses());
		}
		return json_encode(["result" => $result]);
	}
	
	function signUp()
	{
		initDBConnection();
		$result = false;
		if (!getUserByName($_POST['name']))
		{
			$result = true;
			$result = addUser($_POST['name'], base64_encode($_POST['secret']));
		}
		return json_encode(["result" => $result]);
	}
	
	function getPrintableSecret()
	{
		initDBConnection();
		$result = false;

		if (array_key_exists('token', $_SESSION))
		{
			$user = getUserByToken(@$_SESSION['token']);
			if ($user)
			{
				$name 	= substr($user['name'], 0, 4);
				$secret = $user['secret'];
				$result = sendData("getPrintableSecret|{$secret}|{$name}|");
				echo /*strToHex(*/$result/*)*/;die();
			}
		}
		return json_encode(["result" => $result]);
	}
	
	function getWallMessages()
	{
		initDBConnection();
		$result = false;

		if (array_key_exists('token', $_SESSION))
		{
			$user = getUserByToken(@$_SESSION['token']);
			if ($user)
			{
				$result = loadWall($user['id']);
			}
		}
		return json_encode(["result" => $result]);
	}
	
	function getUsersList()
	{
		initDBConnection();
		$result = loadUsersList();
		return json_encode(["result" => $result]);
	}
	
	function addWallMessage()
	{
		initDBConnection();
		$result = false;

		if (array_key_exists('token', $_SESSION))
		{
			$userFrom 	= getUserByToken(@$_SESSION['token']);
			$userTo 	= getUserByName($_POST['to']);
			if ($userFrom && $userTo)
			{
				$result = addWall($userFrom['id'], $userTo['id'], $_POST['text']);
			}
		}
		return json_encode(["result" => $result]);
	}
	
	function get2FAKey()
	{
		$secret = base64_encode($_POST['secret']);
		$data = "getCodeForSecret|{$secret}";
		$result = sendData($data); 
		return json_encode(["result" => $result]);
	}
	
	$url = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
	if ($url == "/user.php" || $url == "/admin.php")
		if (!array_key_exists('token', @$_SESSION))
		{
			header("Location: /"); die();
		}
		
	switch($_GET['cmd'])
	{
		case 'check2FAKey': 		echo check2FAKey(); break;
		case 'get2FAKey': 			echo get2FAKey(); break;
		case 'updateSharedSecret': 	echo updateSharedSecret(); break;
		case 'setSharedSecret': 	echo setSharedSecret(); break;
		case 'getLiveSessions':		echo getListSessions(); break;
		case 'getLoginAttemps':		echo getListLoginAttemps(); break;
		case 'getBackendResponses':	echo getListBackendResponses(); break;
		case 'signUp':				echo signUp(); break;
		case 'getPrintableSecret':	echo getPrintableSecret(); break;
		case 'getWallMessages':		echo getWallMessages(); break;
		case 'addWallMessage':		echo addWallMessage(); break;
		case 'getUsersList':		echo getUsersList(); break;
	}
?>