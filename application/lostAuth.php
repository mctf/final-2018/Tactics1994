<?php
    include_once('config.php')
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Sign in &middot; Tactics 1994</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
	<link href="../assets/css/classic.css" rel="stylesheet" >
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        border: 1px solid #e5e5e5;
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
	.progress {
		height: 20px;
		margin-top: 20px;
		overflow: hidden;
		background-color: #f7f7f7;
		background-image: -moz-linear-gradient(top, #f5f5f5, #f9f9f9);
		background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f5f5f5), to(#f9f9f9));
		background-image: -webkit-linear-gradient(top, #f5f5f5, #f9f9f9);
		background-image: -o-linear-gradient(top, #f5f5f5, #f9f9f9);
		background-image: linear-gradient(to bottom, #f5f5f5, #f9f9f9);
		background-repeat: repeat-x;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		border-radius: 0;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5', endColorstr='#fff9f9f9', GradientType=0);
	}
	.progress .bar {
		float: left;
		width: 0;
		height: 100%;
		font-size: 12px;
		color: #ffffff;
		text-align: center;
		text-shadow: 0;
		background-color: yellow;
		background-image: -moz-linear-gradient(top, yellow, yellow);
		background-image: -webkit-gradient(linear, 0 0, 0 100%, from(yellow), to(yellow));
		background-image: -webkit-linear-gradient(top, yellow, yellow);
		background-image: -o-linear-gradient(top, yellow, yellow);
		background-image: linear-gradient(to bottom, yellow, yellow);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff149bdf', endColorstr='#ff0480be', GradientType=0);
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		-webkit-transition: width 0.6s ease;
		-moz-transition: width 0.6s ease;
		-o-transition: width 0.6s ease;
		transition: width 0.6s ease;
	}
	.progress-danger .bar, .progress .bar-danger {
		background-color: #dd514c;
		background-image: -moz-linear-gradient(top, #ee5f5b, #c43c35);
		background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#c43c35));
		background-image: -webkit-linear-gradient(top, #ee5f5b, #c43c35);
		background-image: -o-linear-gradient(top, #ee5f5b, #c43c35);
		background-image: linear-gradient(to bottom, #ee5f5b, #c43c35);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffc43c35', GradientType=0);
	}
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>
    <div class="container">
		<form id="signinForm" style="display: block;" class="form-signin" action="/actions.php?cmd=check2FAKey" method="POST">
			<h2 class="form-signin-heading">Put here your secret code</h2>
			<input id="secretText" name="secret" type="text" class="input-block-level" placeholder="Secret">
			<div id="lostAuth" class="btn btn-large btn-primary" type="submit">Get OTP</div>
			<div id="codeStuff" style="display:none;">
				<span id="code" style="padding: 0 0;margin: 10px 0 0 0;display: block;" class="center label label-important">Code: REQUESTING...</span>
				<div class="progress progress-warning">
					<div id="progressTimer" class="bar" style="width: 50%;"></div>
				</div>
			</div>
		</form>
		
    </div> <!-- /container -->

    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
	<script src="../assets/js/jquery.columns.min.js"></script>

	<script>
		$("#lostAuth").click(function() {
			$.post("/actions.php?cmd=get2FAKey", { secret: $("#secretText").val(), })
			.done(function( data ) {
				$("#code").text("CODE: " + JSON.parse(data)['result']);
			});
			$("#codeStuff").css("display", "block");
		});
		
		var start, maxTime, timeoutVal;
		function progressBarStart()
		{
		  start = new Date();
		  var currSec = start.getSeconds();
		  if (currSec > 30)
			currSec -= 30;
		  currSec = 30 - currSec;
		  maxTime = currSec * 1000;
		  timeoutVal = Math.floor(maxTime/100);
		  animateUpdate();
		}

		function updateProgress(percentage) {
			$('#progressTimer').css("width", percentage + "%");
		}

		function animateUpdate() {
			var now = new Date();
			var timeDiff = now.getTime() - start.getTime();
			var perc = (Math.round((timeDiff/maxTime)*100) - 100) * -1;
			console.log(perc);
			  if (perc >= 0) {
			   updateProgress(perc);
			   setTimeout(animateUpdate, timeoutVal);
			  } else {
				updateProgress(0);
				progressBarStart();
				$("#code").text("CODE: " + "OUTDATED!");
			  }
		}
	
		$(document).ready(function() {
			progressBarStart();
		});
	</script>
	
  </body>
</html>
